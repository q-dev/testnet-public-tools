# Transaction failed with error: execution reverted

Sometimes when you send a transaction to the Q blockchain, it will fail with the error message `execution reverted` (you can see it on Q explorer). This is a generic error message that can be caused by a number of different issues. This document will explain what `execution reverted` means, what causes it, and how to fix it.

## What does `execution reverted` mean?

When a transaction is sent to the blockchain, it is executed by the EVM (Ethereum Virtual Machine). The EVM is a sandboxed environment that runs the code in the transaction. If the code in the transaction fails, the EVM will revert the transaction and return an error message. The error message will be `execution reverted` if the code in the transaction did not return a more specific error message.

## What causes `execution reverted`?

There are a number of different reasons why a transaction might fail with the error message `execution reverted`. The most common reasons are:

- The transaction ran out of gas
- The transaction was rejected by the smart contract

### The transaction ran out of gas

When a transaction is sent to the blockchain, it is given a gas limit. The gas limit is the maximum amount of gas that the transaction can use. If the transaction runs out of gas before it completes, the EVM will revert the transaction and return an error message. The error message will be `execution reverted` if the code in the transaction did not return a more specific error message.

#### How to detect out of gas issues

You can detect gas limit issues by looking at the transaction in Q explorer. If the transaction has a high value for "Gas Used", it is likely that the transaction ran out of gas. If the transaction has a low value for "Gas Used", it is likely that the transaction was rejected by the smart contract.

[Here's an example](https://explorer.qtestnet.org/tx/0x87084315901e8dbf5b8e0b33816f55d8ec6a80a631aa7daf7c2c2a4f4da8d1d7) of a transaction that ran out of gas.

![Gas Used field in Q Explorer](../img/gas-limit-issue.png)
*Gas Used field in Q Explorer*

#### How to increase gas limit in MetaMask

You can increase the gas limit in MetaMask by clicking on the "Edit" button in the "Gas Limit" field. You can then enter a new value for the gas limit.

This can make the transaction look more expensive (because MetaMask calculates the max fee, based on the gas price and the gas limit). Usually it's not the case, because the transaction costs only the gas it needs. Increasing the limit reduces the risk of running out of gas (thus saving money rather than wasting it, because users have to pay for reverted transactions, too). 

<img src="/img/gas-limit-metamask.gif" alt="Increasing gas limit in MetaMask" width="320"/>
<center>*Increasing gas limit in MetaMask*</center>


#### How to increase gas limit in Q JS SDK

You can explicitly set gas limit or gas buffer for a specific transaction using [`gas` or `gasBuffer` fields](https://q-dev.gitlab.io/q-js-sdk/latest/classes/contracts_BaseContractInstance.BaseContractInstance.html#submitTransaction) in transaction `txOptions`.

Alternatively you can set [`DEFAULT_GASBUFFER` static property](https://q-dev.gitlab.io/q-js-sdk/latest/classes/contracts_BaseContractInstance.BaseContractInstance.html#DEFAULT_GASBUFFER) of `BaseContractInstance` class to increase the gas limit for all transactions sent by the SDK.

### The transaction was rejected by the smart contract

The smart contract that the transaction was sent to may have rejected the transaction. This can happen if the transaction does not meet the requirements of the smart contract. For example, if the transaction is trying to transfer more tokens than the sender has, the transaction will be rejected.

Usually, the smart contract will return a more specific error message than `execution reverted`. However, if the smart contract does not return a more specific error message, the EVM will revert the transaction and return the error message `execution reverted`.
