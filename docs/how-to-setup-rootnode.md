# How to setup a Q Root Node

## Basic Configuration

Clone the repository and go to the `/testnet-rootnode` directory

/// details | Linux
If you use Graphical User Interface(GUI), [open terminal](https://www.geeksforgeeks.org/how-to-open-terminal-in-linux/)

1. Press `CTRL + ALT + T` to open the terminal
2. Or open Show Application in the Dash Bar & find out Terminal Application.
3. Go to the directory where you want to push the repository:
```bash
cd "YOUR_DIRECTORY"
```
You can print your current working directory with:
```bash
pwd
```

4. Clone the repository
```bash
git clone https://gitlab.com/q-dev/testnet-public-tools
```
5. Go to the `/testnet-rootnode` directory
```bash
cd  "testnet-public-tools/testnet-rootnode"
```
///

/// details | Windows (if you don't have git installed)
Open [PowerShell](https://learn.microsoft.com/en-us/powershell/scripting/windows-powershell/starting-windows-powershell?view=powershell-7.5)

1. Press `Windows+R` to open the "Run" tool
2. Type "powershell" and press Enter to open terminal
3. Go to the directory where you want to push the repository:
```bash
cd "YOUR_DISK:\YOUR_DIRECTORY"
```
If you work with cmd instead of PowerShell, use command:
```bash
cd /d "YOUR_DISK:\YOUR_DIRECTORY"
```
Replace YOUR_DISK and YOUR_DIRECTORY with real names.<br>
You can print the current working directory and its contents using the command:
```bash
dir
```


4. Clone the repository:
```
# Download the contents of the Git repository
Invoke-WebRequest -Uri https://gitlab.com/q-dev/testnet-public-tools/-/archive/master/testnet-public-tools-master.zip -OutFile testnet-public-tools-master.zip

# Extract the contents of the ZIP file
Expand-Archive -Path testnet-public-tools-master.zip -DestinationPath .

# Remove the ZIP file
Remove-Item -Path testnet-public-tools-master.zip
```

5. Go to the `/testnet-rootnode` directory

```bash
cd "testnet-public-tools\testnet-rootnode"
```
///

/// details | MacOS
[Open Terminal on Mac](https://support.apple.com/guide/terminal/open-or-quit-terminal-apd5265185d-f365-44cb-8b09-71a064a42125/mac)

1. Click the Launchpad icon in the Dock, type Terminal in the search field, then click Terminal.
2. In the Finder, open the /Applications/Utilities folder, then double-click Terminal.
3. Go to the directory where you want to push the repository:
```bash
cd "YOUR_DIRECTORY"
```
You can print your current working directory with:
```bash
pwd
```

4. Clone the repository
```bash
git clone https://gitlab.com/q-dev/testnet-public-tools
```
5. Go to the `/testnet-rootnode` directory
```bash
cd  "testnet-public-tools/testnet-rootnode"
```
///

<br>

This directory contains the `docker-compose.yaml` file for quick launching of the root node with preconfigurations using `.env` file (which can be created from `.env.example` file).

> **Note:** *If git is not installed on your machine, you can manually copy all files from public repo `testnet-public-tools` onto your machine. Using git is much more comfortable, since it allows pulling file updates with one single command.*

## Set Password for Keystore File

To act as a root node, your node needs a keypair to sign transactions and L0 governance messages. Create a `./keystore` directory and `./keystore/pwd.txt` inside

/// details | Linux, macOS, other Unix-like systems
Your working directory should be `/testnet-rootnode`

1. Create a `./keystore` directory

```bash
mkdir keystore
```
2. Create a `pwd.txt` in a `./keystore` directory and  and set a password that will be used for future account unlocking by entering it into pwd.txt. The password needs to be entered at the beginning of the file.
```bash
touch ./keystore/pwd.txt
nano ./keystore/pwd.txt
```
After entering a password, save the changes in nano:

1. Press `Ctrl+O` to save changes
2. Press `Ctrl+X` to exit
///

/// details | Windows
Your working directory should be `/testnet-rootnode`

1. Create a `./keystore` directory

```bash
md keystore
```
2. Create a `pwd.txt` in a `./keystore` directory and  and set a password that will be used for future account unlocking by entering it into pwd.txt. The password needs to be entered at the beginning of the file.
```
#This will create and open the pwd.txt file in Notepad for editing. If you prefer to use a different text editor, replace notepad.exe with the appropriate command for your editor.
notepad.exe ".\keystore\pwd.txt"
```
///

## Generate a Keypair

Copy `.env.example` to `.env` and in `/testnet-rootnode` directory:

/// details | Linux, macOS, other Unix-like systems
```
cp .env.example .env
```
///

/// details | Windows
```
# This will copy the .env.example file to a new file named .env.
copy ".\env.example" ".\env"
```
///
<br>
Assuming you are in `/testnet-rootnode` directory, issue this command in order to generate a keypair:  

```bash
docker-compose run --rm --entrypoint "geth account new --datadir=/data --password=/data/keystore/pwd.txt" testnet-rootnode
```

The output of this command should look like this:

```text
Your new key was generated

Public address of the key:   0xb3FF24F818b0ff6Cc50de951bcB8f86b52287dac
Path of the secret key file: /data/keystore/UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac

- You can share your public address with anyone. Others need it to interact with you.
- You must NEVER share the secret key with anyone! The key controls access to your funds!
- You must BACKUP your key file! Without the key, it's impossible to access account funds!
- You must REMEMBER your password! Without the password, it's impossible to decrypt the key!
```

This way, a new private key is generated and stored in docker container in `data/keystore` directory encrypted with password from `pwd.txt` file. In our example, *0xb3FF24F818b0ff6Cc50de951bcB8f86b52287DAc* (**you will have a different value**) is the address corresponding to the newly generated private key.

*Alternatively*, you can generate a secret key pair and corresponding file [here](https://vanity-eth.tk/) and save it to the `/keystore` directory manually.
Also you may use `create-geth-private-key.js` script in `/js-tools` folder.

Whether you chose to provide your own vanity keys or use the above command to create a keypair, please ensure that the directory `/keystore` contains the following files:

```text
testnet-rootnode
|   ...
|   ...
└ keystore
  |   UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac
  |   pwd.txt
```

> **Note:** *Following our example, pwd.txt contains the password to encrypted file "UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac" in clear text.*

If you want to change the password in the future, you need to stop the node first.

```bash
docker-compose down
```

Then start password reset procedure with

```bash
docker-compose run testnet-rootnode --datadir /data account update 0xb3ff24f818b0ff6cc50de951bcb8f86b52287dac
```

> **Note:** *You need to remove address _0xb3ff24f818b0ff6cc50de951bcb8f86b52287dac_ and add your account address instead.*

## Configure Node

Edit environment file in `/testnet-rootnode` directory:

/// details | Linux, macOS, other Unix-like systems

```bash
nano .env
```
After editing `.env`, save the changes in nano:

1. Press `Ctrl+O` to save changes
2. Press `Ctrl+X` to exit
///

/// details | Windows

```
#This will open the .env file in Notepad for editing. If you prefer to use a different text editor, replace notepad.exe with the appropriate command for your editor.
notepad.exe .\env
```
///
<br>
Enter your (newly created) root node address without leading 0x here:

```text
# your q address here (without leading 0x)
ADDRESS=b3FF24F818b0ff6Cc50de951bcB8f86b52287DAc
```

Then add your machines public IP address (please make sure your machine is reachable at the corresponding IP since it's required for discoverability by other network participants) here:

```text
# your public IP address here
IP=193.19.228.94
```

Optionally choose a port for p2p protocol or just leave default value (use different ports for every node you are running):

```text
# the port you want to use for p2p communication (default is 30313)
EXT_PORT=30313
```

The resulting `.env` file should look somehow like this:

```text
# docker image for q client
QCLIENT_IMAGE=qblockchain/q-client:v1.4.0

# your q address here (without leading 0x)
ADDRESS=b3FF24F818b0ff6Cc50de951bcB8f86b52287DAc

# your public IP address here
IP=193.19.228.94

# the port you want to use for p2p communication (default is 30313)
EXT_PORT=30313

# extra bootnode you want to use
BOOTNODE1_ADDR=enode://88ba6bd2d11752c42f3a4bb038f4eee35456c0cfc3484305d4a35c5072b3c6fb6a1ab6553ad8330f3de119724076e0e498d1af4d9175befe6ba4583a6b99c0dd@bootnode.qtestnet.org:30301
BOOTNODE2_ADDR=enode://c610793186e4f719c1ace0983459c6ec7984d676e4a323681a1cbc8a67f506d1eccc4e164e53c2929019ed0e5cfc1bc800662d6fb47c36e978ab94c417031ac8@extrabootnode.qtestnet.org:30304
BOOTNODE3_ADDR=enode://7a8ade64b79961a7752daedc4104ca4b79f1a67a10ea5c9721e7115d820dbe7599fe9e03c9c315081ccf6a2afb0b6652ee4965e38f066fe5bf129abd6d26df58@extrabootnode.qtestnet.org:30306
```

## Add your Root Node to https://stats.qtestnet.org

If you want your root node to report to the [network statistics](https://stats.qtestnet.org), you can add an additional flag to the node entrypoint within file `/testnet-rootnode/docker-compose.yaml`, it should look like this:

```yaml
testnet-rootnode:
  image: $QCLIENT_IMAGE
  entrypoint: ["geth", "--ethstats=<Your_RootNode_Name>:<Testnet_access_key>@stats.qtestnet.org", "--datadir=/data", ...]
```

`<Your_RootNode_Name>` can be chosen arbitrarily. It will be displayed in the statistics and could be something like "OurCoolCompany - 0xABC123". You can use special characters, emojis as well as spaces. We would appreciate to include the beginning of your Root Node Q address, so there is a link between your client and your address.

In order to find out the `<Testnet_access_key>` please join [Q Discord Server](https://discord.gg/YTgkvJvZGD) and find stats key in [**🔑│testnet-key** channel](https://discord.com/channels/902893347239247952/1042401601639432212).

## Launch Root Node

Now launch your root node using docker-compose file in rootnode directory:

```bash
docker-compose up -d
```

Check your nodes real-time logs with the following command:

```bash
docker-compose logs -f --tail "100"
```

## Find additional peers

In case your client can't connect with the default configuration, we recommend that you add an additional flag referring to one of our additional peers (`$BOOTNODE1_ADDR`, `$BOOTNODE2_ADDR`or `$BOOTNODE3_ADDR`) within `docker-compose.yaml` file:

```yaml
testnet-rootnode:
  image: $QCLIENT_IMAGE
  entrypoint: ["geth", "--bootnodes=$BOOTNODE1_ADDR,$BOOTNODE2_ADDR,$BOOTNODE3_ADDR", "--datadir=/data", ...]
```

## Get Q Tokens

In order to become a root node, you will need to make an onchain proposal to [add yourself to the root node panel](how-to-become-a-root-node.md). You need Q tokens for this. For Q testnet, you can get some Q using the [faucet](https://faucet.qtestnet.org/). Check the [faucet documentation](https://docs.qtestnet.org/how-to-install-metamask/#faucet) for more information. Finally, please verify that tokens were sent by looking up your address within [Block Explorer](https://explorer.qtestnet.org/).

## Put Stake in Rootnodes Contract

As was mentioned previously, you should put stake to rootnodes contract in order to become a rootnode.

You can use the dApp "Your HQ" that can be found at [https://hq.qtestnet.org](https://hq.qtestnet.org). Go to `Staking` -> `Root Node Staking` for stake management. Also, you may want to check our [Staking documentation](dapp-staking.md).

## Updating Q-Client & Docker Images

To upgrade the node follow the instructions [Upgrade Node](how-to-upgrade-node.md)
