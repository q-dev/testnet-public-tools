## Updating Q-Client & Docker Images

In case of severe updates of the Q-Client, you will be required to update the validator files and configs. To do so, within directory `/testnet-validator` (for validator), `/testnet-rootnode` (for rootnode) or `/testnet-fullnode` (for fullnode), use the following commands:

1. Go to directory `/testnet-validator` or `/testnet-rootnode` or `/testnet-fullnode`
/// details | Linux, macOS, other Unix-like systems

```bash
cd  "YOUR_DISK:/YOUR_DIRECTORY/testnet-public-tools/testnet-validator" # or `/testnet-rootnode` or `/testnet-fullnode`
```
///

/// details | Windows

```bash
cd /d "YOUR_DISK:\YOUR_DIRECTORY\testnet-public-tools\testnet-validator" # or `/testnet-rootnode` or `/testnet-fullnode`
```
///
<br>
2. Change the docker image directly in your `.env` file:
/// details | Linux, macOS, other Unix-like systems

```bash
nano .env
```
After editing `.env`, save the changes in nano:

1. Press `Ctrl+O` to save changes
2. Press `Ctrl+X` to exit
///

/// details | Windows

```
#This will open the .env file in Notepad for editing. If you prefer to use a different text editor, replace notepad.exe with the appropriate command for your editor.
notepad.exe .\env
```
///
<br>
```
...
QCLIENT_IMAGE=qblockchain/q-client:v1.4.0
...
```

3. Pull (and overwrite) the latest docker image 
```bash
docker-compose pull
```

4. Restart with new configs & images
```bash
docker-compose down -t 60 && docker-compose up -d
```

Now your validator node should restart and synchronise with the testnet again.

