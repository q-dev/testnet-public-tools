# How to setup a Q Validator Node

## Setup your Server

You must prepare your server / machine to begin. One possibility is to use a local machine, alternatively you can use a cloud instance on AWS for example. There is a good external tutorial on how to get started with Ethereum on AWS. You can use this [tutorial](https://medium.com/@pilankar.akshay3/how-to-setup-a-ethereum-poa-private-proof-of-authority-ethereum-network-network-on-amazon-aws-5fdf56d2ad93) as a basic reference.

Any Linux machine will work if it meets the following requirements:

- Linux machine with SSH access;
- Installed applications: docker, docker-compose, git (optional).

Guide how to install [docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04)
and [docker-compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-22-04) could be found
by the links.

#### Hardware Requirments

| | **Validator Node** |
|:--|:--:|
| Disk | Min. 85 GB **free** disk space <br> Rec. 160 GB **free** disk space|
| Memory | Min. 6 GB RAM <br> Rec. 8 GB RAM |
| CPU | Min. 1(v) Core(x86) <br> Rec. 2(v) Cores(x86) |
| Bandwidth | 24Mbps+ |

## Basic Configuration

Clone the repository and go to the `/testnet-validator` directory

/// details | Linux
If you use Graphical User Interface(GUI), [open terminal](https://www.geeksforgeeks.org/how-to-open-terminal-in-linux/)

1. Press `CTRL + ALT + T` to open the terminal
2. Or open Show Application in the Dash Bar & find out Terminal Application.
3. Go to the directory where you want to push the repository:
```bash
cd "YOUR_DIRECTORY"
```
You can print your current working directory with:
```bash
pwd
```

4. Clone the repository
```bash
git clone https://gitlab.com/q-dev/testnet-public-tools
```
5. Go to the `/testnet-validator` directory
```bash
cd  "testnet-public-tools/testnet-validator"
```
///


/// details | Windows (if you don't have git installed)
Open [PowerShell](https://learn.microsoft.com/en-us/powershell/scripting/windows-powershell/starting-windows-powershell?view=powershell-7.5)

1. Press `Windows+R` to open the "Run" tool
2. Type "powershell" and press Enter to open terminal
3. Go to the directory where you want to push the repository:
```bash
cd "YOUR_DISK:\YOUR_DIRECTORY"
```
If you work with cmd instead of PowerShell, use command:
```bash
cd /d "YOUR_DISK:\YOUR_DIRECTORY"
```
Replace YOUR_DISK and YOUR_DIRECTORY with real names.<br>
You can print the current working directory and its contents using the command:
```bash
dir
```


4. Clone the repository:
```
# Download the contents of the Git repository
Invoke-WebRequest -Uri https://gitlab.com/q-dev/testnet-public-tools/-/archive/master/testnet-public-tools-master.zip -OutFile testnet-public-tools-master.zip

# Extract the contents of the ZIP file
Expand-Archive -Path testnet-public-tools-master.zip -DestinationPath .

# Remove the ZIP file
Remove-Item -Path testnet-public-tools-master.zip
```

5. Go to the `/testnet-validator` directory

```bash
cd  "testnet-public-tools\testnet-validator"
```

///

/// details | MacOS
[Open Terminal on Mac](https://support.apple.com/guide/terminal/open-or-quit-terminal-apd5265185d-f365-44cb-8b09-71a064a42125/mac)

1. Click the Launchpad icon in the Dock, type Terminal in the search field, then click Terminal.
2. In the Finder, open the /Applications/Utilities folder, then double-click Terminal.
3. Go to the directory where you want to push the repository:
```bash
cd "YOUR_DIRECTORY"
```
You can print your current working directory with:
```bash
pwd
```

4. Clone the repository
```bash
git clone https://gitlab.com/q-dev/testnet-public-tools
```
5. Go to the `/testnet-validator` directory
```bash
cd  "testnet-public-tools/testnet-validator"
```
///

<br>
This directory contains the `docker-compose.yaml` file for quick launching of the validator node with preconfigurations on rpc, blockchain explorer using `.env` (which can be created from `.env.example`).

> **Note:** *If git is not installed on your machine, you can manually copy all files from public repo `testnet-public-tools` onto your machine. Using git is much more comfortable, since it allows pulling file updates with one single command.*

## Generate a Keypair for Validator

Copy `.env.example` to `.env` and in `/testnet-validator` directory:

/// details | Linux, macOS, other Unix-like systems
```
cp .env.example .env
```
///

/// details | Windows
```
# This will copy the .env.example file to a new file named .env.
copy ".\env.example" ".\env"
```
///
<br>
In order to sign blocks and receive reward, a validator needs a keypair.
Create a `./keystore` directory, then a password which will be used for private key encryption and save it into a text file `pwd.txt` in `./keystore` directory.
/// details | Linux, macOS, other Unix-like systems
Your working directory should be `/testnet-validator`

1. Create a `./keystore` directory

```bash
mkdir keystore
```
2. Create a `pwd.txt` in a `./keystore` directory and  and set a password that will be used for future account unlocking by entering it into pwd.txt. The password needs to be entered at the beginning of the file. 
```bash
touch ./keystore/pwd.txt
nano ./keystore/pwd.txt
```
After entering a password, save the changes in nano:

1. Press `Ctrl+O` to save changes
2. Press `Ctrl+X` to exit
///

/// details | Windows
Your working directory should be `/testnet-validator`

1. Create a `./keystore` directory

```bash
md keystore
```
2. Create a `pwd.txt` in a `./keystore` directory and  and set a password that will be used for future account unlocking by entering it into pwd.txt. The password needs to be entered at the beginning of the file.
```
#This will create and open the pwd.txt file in Notepad for editing. If you prefer to use a different text editor, replace notepad.exe with the appropriate command for your editor.
notepad.exe ".\keystore\pwd.txt"
```
///
<br>
Assuming you are in `/testnet-validator` directory, issue this command in order to generate a keypair:  

```bash
docker-compose run --rm --entrypoint "geth account new --datadir=/data --password=/data/keystore/pwd.txt" testnet-validator-node
```


The output of this command should look like this:

```text
Your new key was generated

Public address of the key:   0xb3FF24F818b0ff6Cc50de951bcB8f86b52287dac
Path of the secret key file: /data/keystore/UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac

- You can share your public address with anyone. Others need it to interact with you.
- You must NEVER share the secret key with anyone! The key controls access to your funds!
- You must BACKUP your key file! Without the key, it's impossible to access account funds!
- You must REMEMBER your password! Without the password, it's impossible to decrypt the key!
```

This way, a new private key is generated and stored in `/keystore` directory encrypted with password from `pwd.txt` file. In our example, *0xb3FF24F818b0ff6Cc50de951bcB8f86b52287DAc* (**you will have a different value**) is the address corresponding to the newly generated private key.

Alternatively, you can generate a secret key pair and according file on this [page](https://vanity-eth.tk/) and save it to the `/keystore` directory manually.
Also you may use `create-geth-private-key.js` script in `/js-tools` folder.

Whether you chose to provide your own vanity keys or use the above command to create a keypair, please ensure that the directory `/keystore` contains the following files:

```text
testnet-validator
|   ...
|   ...
└ keystore
  |   UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac
  |   pwd.txt
```

> **Note:** *Following our example, pwd.txt contains the password to encrypted file "UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac" in clear text.*

If you want to change the password in the future, you need to stop the node first.

```bash
docker-compose down
```

Then start password reset procedure with

```bash
docker-compose run testnet-validator-node --datadir /data account update 0xb3ff24f818b0ff6cc50de951bcb8f86b52287dac
```

> **Note:** *You need to remove address _0xb3ff24f818b0ff6cc50de951bcb8f86b52287dac_ and add your account address instead.*

## Get Q Tokens

In order to become a validator, you will need to put some stake in validators contract, so you need Q tokens for this. For Q testnet, you can get some Q using the [faucet](https://faucet.qtestnet.org/). Check the [faucet documentation](https://docs.qtestnet.org/how-to-install-metamask/#faucet) for more information. Finally, please verify that tokens were sent by looking up your address within [Block Explorer](https://explorer.qtestnet.org/).

## Configure Setup

Edit environment file in `/testnet-validator` directory:

/// details | Linux, macOS, other Unix-like systems

```bash
nano .env
```
After editing `.env`, save the changes in nano:

1. Press `Ctrl+O` to save changes
2. Press `Ctrl+X` to exit
///

/// details | Windows

```
#This will open the .env file in Notepad for editing. If you prefer to use a different text editor, replace notepad.exe with the appropriate command for your editor.
notepad.exe .\env
```
///
<br>
Put your address without leading 0x from the step [Generate a Keypair for Validator](/how-to-setup-validator/#generate-a-keypair-for-validator), into `ADDRESS`, your public IP address (please make sure your machine is reachable at the corresponding IP) into `IP` (this is required for discoverability by other network participants) and optionally choose a port for p2p protocol (or just leave default value). The resulting `.env` file should look like this:


```text
# docker image for q client
QCLIENT_IMAGE=qblockchain/q-client:v1.4.0

# your q address here (without leading 0x)
ADDRESS=b3FF24F818b0ff6Cc50de951bcB8f86b52287DAc

# your public IP address here
IP=193.19.228.94

# the port you want to use for p2p communication (default is 30313)
EXT_PORT=30313

# extra bootnode you want to use
BOOTNODE1_ADDR=enode://88ba6bd2d11752c42f3a4bb038f4eee35456c0cfc3484305d4a35c5072b3c6fb6a1ab6553ad8330f3de119724076e0e498d1af4d9175befe6ba4583a6b99c0dd@bootnode.qtestnet.org:30301
BOOTNODE2_ADDR=enode://c610793186e4f719c1ace0983459c6ec7984d676e4a323681a1cbc8a67f506d1eccc4e164e53c2929019ed0e5cfc1bc800662d6fb47c36e978ab94c417031ac8@extrabootnode.qtestnet.org:30304
BOOTNODE3_ADDR=enode://7a8ade64b79961a7752daedc4104ca4b79f1a67a10ea5c9721e7115d820dbe7599fe9e03c9c315081ccf6a2afb0b6652ee4965e38f066fe5bf129abd6d26df58@extrabootnode.qtestnet.org:30306
```

Next, you need to edit `config.json` as this file is required for staking. Put your address from above into the address field and password from `./keystore/pwd.txt` into the password field. 

/// details | Linux, macOS, other Unix-like systems

```bash
nano config.json
```
After editing `config.json`, save the changes in nano:

1. Press `Ctrl+O` to save changes
2. Press `Ctrl+X` to exit
///

/// details | Windows

```
#This will open the config.json file in Notepad for editing. If you prefer to use a different text editor, replace notepad.exe with the appropriate command for your editor.
notepad.exe .\config.json
```
///
<br>
Resulting `config.json` should be similar to this:

```json
    {
      "address": "b3FF24F818b0ff6Cc50de951bcB8f86b52287DAc",
      "password": "supersecurepassword",
      "keystoreDirectory": "/data",
      "rpc": "https://rpc.qtestnet.org"
    }
```

## Put Stake in Validators Contract

As was mentioned previously, you need to put stake to validators contract in order to become a validator.

You can use the dApp "Your HQ" that can be found at [https://hq.qtestnet.org](https://hq.qtestnet.org). Ultimately, you need to `Join Validator Ranking` to receive rewards. The according functionality is located at `Staking -> Validator Staking`.

## Add your Validator to https://stats.qtestnet.org

If you want your validator to report to the [network statistics](https://stats.qtestnet.org), you can add an additional flag to the node entrypoint within file `/testnet-validator/docker-compose.yaml`, it should look like this:

```yaml
testnet-validator-node:
  image: $QCLIENT_IMAGE
  entrypoint: ["geth", "--ethstats=<Your_Validator_Name>:<Testnet_access_key>@stats.qtestnet.org", "--datadir=/data", ...]
```

`<Your_Validator_Name>` can be chosen arbitrarily. It will be displayed in the statistics. If you want to disclose your ID, this could be something like "OurCoolCompany - Don't trust, verify". You can use special characters, emojis as well as spaces. If you prefer to stay anonymous, we would appreciate to include the beginning of your validator Q address, so there is a link between your client and your address.

In order to find out the `<Testnet_access_key>` please join [Q Discord Server](https://discord.gg/YTgkvJvZGD) and find stats key in [**🔑│testnet-key** channel](https://discord.com/channels/902893347239247952/1042401601639432212).

## Launch Validator Node

Now launch your validator node using docker-compose file in `/testnet-validator` directory:

```bash
docker-compose up -d
```

You can check your nodes real-time logs with the following command:

```bash
docker-compose logs -f --tail "100"
```

To stop viewing logs, press `Ctrl+C`

## Find additional peers

In case your client can't connect with the default configuration, we recommend that you add an additional flag referring to one of our additional peers (`$BOOTNODE1_ADDR`, `$BOOTNODE2_ADDR`or `$BOOTNODE3_ADDR`) within `docker-compose.yaml` file:

```yaml
testnet-validator-node:
  image: $QCLIENT_IMAGE
  entrypoint: ["geth", "--bootnodes=$BOOTNODE1_ADDR,$BOOTNODE2_ADDR,$BOOTNODE3_ADDR", "--datadir=/data", ...]
```

## Verify that Node is producing Blocks

In order for you to start validating, you must wait for the new epoch (i.e. validation cycle). If everything went correctly before and the committed stake was sufficient to enter the validator shortlist, your validator node will start to produce blocks in the next validation cycle.
Please note that upon start you are likely to see a lot of warnings in q-client logs:

```text
WARN [01-18|13:12:00.431] Block sealing failed          err="unauthorized signer"
```

This is actually ok, as the node needs some time to synchronize with the peers of Q network. Until a full sync is reached, it may happen that your node already starts block creation using the most recent snapshot in which you are the only validator. After successful peer discovery, there warnings will disappear.

  > **Note:** *At the time testnet validators are not required to run an omnibridge-oracle. If you want to run it anyway please see [here](how-to-setup-omnibridge.md#Configure-OmniBridge-Oracle) for a tutorial how to do this.*

## Exit the Validator Ranking

Go to the dApp "Your HQ" at [https://hq.qtestnet.org](https://hq.qtestnet.org), connect it with your validator node wallet account and go to `Announce Withdrawal` within `Staking -> Validator Staking -> Validator Manage` of 100% of your self-staked Q token. After doing that, you will be taken out of the ranking immediately, though your node might still validate blocks until the next validation cycle begins (within max. 8 minutes).

The announced amount will be put on an escrow balance for a certain time (see constitution parameter `constitution.valWithdrawP`) until it can be withdrawn fully. Re-joining the panel is possible any time by putting back stake or reducing the announced withdrawal amount.

  > **Note:** *A temporary exit from the ranking is possible as described above. For re-entering, you need to announce withdrawal of `0` Q which overwrites your initial announcement and restores your self-stake to 100%. You need to `Join Validator Ranking` again to finalise the re-entering procedure. A temporary exit (or pause) might be useful if you are planning a maintenance downtime of your node for example.*

## Updating Q-Client & Docker Images

To upgrade the node follow the instructions [Upgrade Node](how-to-upgrade-node.md)
