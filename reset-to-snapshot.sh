#! /bin/sh

# you can use this script to reset the blockchain data to the latest stakecraft snapshot
# This can be useful for faster catchup (instead of syncing from genesis block)
# or to rewind to a stable state when your own chain data is corrupt or your node is stuck otherwise.

# reset fullnode:
#    cd testnet-fullnode
#    docker-compose cp ../reset-to-snapshot.sh testnet-fullnode:/data
#    docker-compose run --rm --entrypoint /data/reset-to-snapshot.sh testnet-fullnode
# reset validator:
#    cd testnet-validator
#    docker-compose cp ../reset-to-snapshot.sh testnet-validator-node:/data
#    docker-compose run --rm --entrypoint /data/reset-to-snapshot.sh testnet-validator-node
# reset root node:
#    cd testnet-rootnode
#    docker-compose cp ../reset-to-snapshot.sh testnet-rootnode:/data
#    docker-compose run --rm --entrypoint /data/reset-to-snapshot.sh testnet-rootnode


rm -rf /data/geth/chaindata
mkdir -p /data/geth/chaindata
cd /data/geth/chaindata
wget -O - https://snapshots.stakecraft.com/q-testnet-latest.tar | tar xf -
